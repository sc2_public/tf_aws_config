# aws_config_phi

Sets AWS Config Rules for a Stanford AWS PHI account, by

* Creating rules for storage objects and IAM
* Delivering configuration changes for those rules to a central aggregated bucket
* Designed to be used with the aws_account_* modules

## Recorded Resources

Currently, the configurations of the following resources types are recorded in all regions (see `region_resource_types` in  _variables.tf_)

  * EC2 Security Groups
  * EBS Volumes
  * S3 Buckets
  * RDS DBInstances

Additionally, since IAM is a global service, IAM Users are recorded in in the _us-west-2_ (Oregon) region.

## Rules

The following rules are activated, using pre-defined AWS checks. The names below are the names of the Terraform resources, not the AWS check name.

  * `ebs_encrypted` - check for mounted, unencrypted EBS volumes
  * `s3_encrypted` - check for unencrypted S3 buckets
  * `efs_encrypted` - check for unencrypted EFS filesystems
  * `rds_encrypted` - check for unencrypted RDS storage
  * `rds_backups` - checks that backups are enabled for RDS instances
  * `rds_private` - checks that RDS instances are only accessible from within their VPC
  * `rds_ha` - checks that RDS instances are multi-az for redundancy
  * `ssh_unrestricted` - checks whether security groups allow SSH from anywhere
  * `iam_mfa_console` - checks that IAM console users have MFA enabled
  * `iam_unused_creds` - checks for IAM users with passwords or access keys that have not been used for 90 days
  * `iam_keys_rotated` - checks for active IAM access keys that are more than 90 days old



  