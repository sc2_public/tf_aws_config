# variables for config setup

data "aws_region" "current" { }

data "aws_iam_role" "audit" {
  name = "${var.audit_role_name}"
}

variable "audit_role_name" {
  description = "Name of audit role that has been created in account"
  type        = "string"
}

variable "data_class" {
  type        = "string"
  description = "Data Risk Classification (default, highrisk, phi)"
  default     = "default"
}

variable "aggregator_id" {
  type        = "string"
  description = "AWS Account ID to authorize for Config Aggregation"
  default     = "none"
}

variable "aggregator_region" {
  type        = "string"
  description = "AWS Region in which the Config Aggregator runs"
  default     = "us-west-2"
}

variable "aggregator_bucket" {
  type        = "string"
  description = "AWS S3 bucket for config recorder"
  default     = "none"
}
  
variable "regional_resource_types" {
  type        = "map"
  description = "List of resource types to record in each region"
  default     = {
    # We also use us-west-2 to handles global resources, like IAM
    # if this changes, also update the iam_config_rules_region variable below
    "us-west-2" = [
      "AWS::IAM::User",
      "AWS::EC2::SecurityGroup",
      "AWS::EC2::Volume",
      "AWS::RDS::DBInstance",
      "AWS::S3::Bucket"
    ],
    "us-west-1" = [
      "AWS::EC2::SecurityGroup",
      "AWS::EC2::Volume",
      "AWS::RDS::DBInstance",
      "AWS::S3::Bucket"
    ]
    "us-east-2" = [
      "AWS::EC2::SecurityGroup",
      "AWS::EC2::Volume",
      "AWS::RDS::DBInstance",
      "AWS::S3::Bucket"
    ],
    "us-east-1" = [
      "AWS::EC2::SecurityGroup",
      "AWS::EC2::Volume",
      "AWS::RDS::DBInstance",
      "AWS::S3::Bucket"
    ]
  }
}

variable "iam_unused_creds_days" {
  type        = "string"
  description = "max days IAM creds can be unused"
  default     = 90
}

variable "iam_key_rotation_days" {
  type        = "string"
  description = "max days for IAM keys before they must be rotated"
  default     = 90
}

variable "config_rule_enablement" {
  type        = "map"
  description = "Defines which config rules are enabled"
  default = {
    "ebs_encrypted"    = 1,
    "s3_encrypted"     = 1,
    "efs_encrypted"    = 1,
    "rds_encrypted"    = 1,
    "rds_backups"      = 0,
    "rds_private"      = 0,
    "rds_ha"           = 0,
    "iam_mfa_console"  = 1,
    "iam_unused_creds" = 0,
    "iam_keys_rotated" = 0,
    "ssh_unrestricted" = 0
  }
}

variable "iam_config_rules_region" {
  description = "Region in which to create IAM-related config rules"
  type        = "string"
  default     = "us-west-2"
}


locals {
  region    = "${data.aws_region.current.name}"
  phi_count = "${var.data_class == "phi" ? 1 : 0}"
  iam_count = "${data.aws_region.current.name == var.iam_config_rules_region ? 1 : 0}"
}

locals {
  ebs_encrypted    = "${var.config_rule_enablement["ebs_encrypted"]    * local.phi_count}"
  s3_encrypted     = "${var.config_rule_enablement["s3_encrypted"]     * local.phi_count}"
  efs_encrypted    = "${var.config_rule_enablement["efs_encrypted"]    * local.phi_count}"
  rds_encrypted    = "${var.config_rule_enablement["rds_encrypted"]    * local.phi_count}"
  rds_backups      = "${var.config_rule_enablement["rds_backups"]      * local.phi_count}"
  rds_private      = "${var.config_rule_enablement["rds_private"]      * local.phi_count}"
  rds_ha           = "${var.config_rule_enablement["rds_ha"]           * local.phi_count}"
  ssh_unrestricted = "${var.config_rule_enablement["ssh_unrestricted"] * local.phi_count}"

  iam_mfa_console  = "${var.config_rule_enablement["iam_mfa_console"]  * local.phi_count * local.iam_count }"
  iam_unused_creds = "${var.config_rule_enablement["iam_unused_creds"] * local.phi_count * local.iam_count }"
  iam_keys_rotated = "${var.config_rule_enablement["iam_keys_rotated"] * local.phi_count * local.iam_count }"
}
