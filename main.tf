#
# AWS Config
#

/*
 * Config Recorder, Delivery Channel, and Aggregator configuration
 */

resource "aws_config_configuration_recorder" "default" {
  name     = "${local.region}"
  role_arn = "${data.aws_iam_role.audit.arn}"
  recording_group {
    all_supported = false
    resource_types = [ "${var.regional_resource_types[local.region]}" ]
  }
}

/*
 * config data is delivered to a shared bucket in another account
 */
resource "aws_config_delivery_channel" "default" {
  name           = "${local.region}"
  s3_bucket_name = "${var.aggregator_bucket}"
  depends_on     = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_configuration_recorder_status" "default" {
  name       = "${local.region}"
  is_enabled = "${var.data_class == "phi" ? true : false}"
  depends_on = [
    "aws_config_delivery_channel.default",
    "aws_config_configuration_recorder.default",
  ]
}

resource "aws_config_aggregate_authorization" "default" {
  account_id = "${var.aggregator_id}"
  region     = "${var.aggregator_region}"
}

/*
 * Encrypted Storage
 */
resource "aws_config_config_rule" "ebs_encrypted" {
  count       = "${local.ebs_encrypted}"
  name        = "ebs-encrypted"
  description = "Checks whether EBS volumes (attached to EC2 instances) are encrypted."

  source {
    owner             = "AWS"
    source_identifier = "ENCRYPTED_VOLUMES"
  }

  scope {
    compliance_resource_types = [ "AWS::EC2::Volume" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "s3_encrypted" {
  count       = "${local.s3_encrypted}"
  name        = "s3-encrypted"
  description = "Checks whether S3 buckets are encrypted."

  source {
    owner             = "AWS"
    source_identifier = "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED"
  }

  scope {
    compliance_resource_types = [ "AWS::S3::Bucket" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "efs_encrypted" {
  count       = "${local.efs_encrypted}"
  name        = "efs-encrypted"
  description = "Checks whether Amazon EFS are configured to encrypt file data using AWS KMS."

  source {
    owner             = "AWS"
    source_identifier = "EFS_ENCRYPTED_CHECK"
  }

  maximum_execution_frequency = "TwentyFour_Hours"

  depends_on = ["aws_config_configuration_recorder.default"]
}

/*
 * RDS rules - encrypted, backed up, not-public, HA
 */

resource "aws_config_config_rule" "rds_encrypted" {
  count       = "${local.rds_encrypted}"
  name        = "rds-encrypted"
  description = "Checks whether storage encryption is enabled for your RDS DB instances."

  source {
    owner             = "AWS"
    source_identifier = "RDS_STORAGE_ENCRYPTED"
  }

  scope {
    compliance_resource_types = [ "AWS::RDS::DBInstance" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "rds_backups" {
  count       = "${local.rds_backups}"
  name        = "rds-backups"
  description = "Checks whether RDS DB instances have backups enabled."

  source {
    owner             = "AWS"
    source_identifier = "DB_INSTANCE_BACKUP_ENABLED"
  }

  scope {
    compliance_resource_types =  [ "AWS::RDS::DBInstance" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "rds_private" {
  count       = "${local.rds_private}"
  name        = "rds-private"
  description = "Checks whether RDS instances are not publicly accessible."

  source {
    owner             = "AWS"
    source_identifier = "RDS_INSTANCE_PUBLIC_ACCESS_CHECK"
  }

  scope {
    compliance_resource_types = [ "AWS::RDS::DBInstance" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "rds_ha" {
  count       = "${local.rds_ha}"
  name        = "rds_ha"
  description = "Checks whether high availability is enabled for RDS instances."

  source {
    owner             = "AWS"
    source_identifier = "RDS_MULTI_AZ_SUPPORT"
  }

  scope {
    compliance_resource_types = [ "AWS::RDS::DBInstance" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

/*
 * EC2 Rules
 */
resource "aws_config_config_rule" "ssh_unrestricted" {
  count       = "${local.ssh_unrestricted}"
  name        = "ssh-unrestricted"
  description = "Checks whether active security groups disallow unrestricted incoming SSH traffic."

  source {
    owner             = "AWS"
    source_identifier = "INCOMING_SSH_DISABLED"
  }

  scope {
    compliance_resource_types = [ "AWS::EC2::SecurityGroup" ]
  }

  depends_on = ["aws_config_configuration_recorder.default"]
}

/*
 * IAM Rules
 */
resource "aws_config_config_rule" "iam_mfa_console" {
  count       = "${local.iam_mfa_console}"
  name        = "iam-mfa-console"
  description = "Checks whether MFA is enabled for all IAM users have console access."

  source {
    owner             = "AWS"
    source_identifier = "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS"
  }

  maximum_execution_frequency = "TwentyFour_Hours"

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "iam_unused_creds" {
  count       = "${local.iam_unused_creds}"
  name        = "iam-unused-creds"
  description = "Checks whether IAM users have passwords or active access keys that have not been used within 90 days."

  source {
    owner             = "AWS"
    source_identifier = "IAM_USER_UNUSED_CREDENTIALS_CHECK"
  }

  input_parameters = "{\"maxCredentialUsageAge\":\"${var.iam_unused_creds_days}\"}"

  maximum_execution_frequency = "TwentyFour_Hours"

  depends_on = ["aws_config_configuration_recorder.default"]
}

resource "aws_config_config_rule" "iam_keys_rotated" {
  count       = "${local.iam_keys_rotated}"
  name        = "iam-keys-rotated"
  description = "Checks whether active access keys are rotated within 90 days."

  source {
    owner             = "AWS"
    source_identifier = "ACCESS_KEYS_ROTATED"
  }

  input_parameters = "{\"maxAccessKeyAge\":\"${var.iam_key_rotation_days}\"}"

  maximum_execution_frequency = "TwentyFour_Hours"

  depends_on = ["aws_config_configuration_recorder.default"]
}

